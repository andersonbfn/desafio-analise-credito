package com.andersonbfn.desafio.exceptionhandlers;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.Logger;

import io.quarkus.security.ForbiddenException;
import io.quarkus.security.UnauthorizedException;
import io.smallrye.mutiny.TimeoutException;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;

public class ExceptionHandlers  {
  private static final Logger log = Logger.getLogger(ExceptionHandlers.class);

  @Provider
  public static class DefaultExceptionMapper implements ExceptionMapper<Exception> {
	@Override
    public Response toResponse(Exception exception) {
      if (exception instanceof ClientErrorException) {
        ClientErrorException clientError = (ClientErrorException) exception;
        log.warn(clientError.toString());

        if (clientError instanceof BadRequestException) {
          final BadRequestException badRequest = (BadRequestException) clientError;
          final String mensagem = StringUtils.isEmpty(exception.getMessage()) 
              ? "Os parâmetros da requisição estão em desacordo com a documentação." : exception.getMessage();
          return Response.status(Status.BAD_REQUEST).entity(new JsonObject().put("message", mensagem)).build();
        }
        
        // 429 Too Many Requests
        if (clientError.getResponse().getStatus() == Status.TOO_MANY_REQUESTS.getStatusCode()) {
          return Response.status(Status.TOO_MANY_REQUESTS)
        	  .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
              .entity(new JsonObject().put("message", "O limite de requests foi atingido")).build();
        }
        
        final String mensagem = StringUtils.isEmpty(clientError.getMessage()) 
            ? "Requisição inválida." : clientError.getMessage();
        return Response.status(clientError.getResponse().getStatus()) 
        		.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
        		.entity(new JsonObject().put("message", mensagem))
        		.build();

      }
      
      if (exception instanceof DecodeException) {
		return Response.status(Status.BAD_REQUEST).entity(new JsonObject()
				.put("message", "Erro ao traduzir json. Certifique-se que o conteúdo como campos e valores como 'date/time' "
						+ "estejam convertidos para String (ex. com uso de Stringfy)"))
				.build();
      }
      
      if (exception instanceof ServerErrorException) {
        ServerErrorException serverError = (ServerErrorException) exception;
        log.error(serverError.toString());
        final String mensagem = StringUtils.isEmpty(exception.getMessage()) 
            ? "Erro ao processar requisição. Contate o suporte." : exception.getMessage();
        final JsonObject jsonResponse = new JsonObject().put("message", mensagem);
        if (serverError.getCause() != null) {
        	jsonResponse.put("cause", serverError.getCause().getMessage());
		}
		return Response.status(serverError.getResponse().getStatus())
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.entity(jsonResponse).build();
      }
      
      // IllegalStateException
      if (exception instanceof IllegalStateException) {
        final IllegalStateException illegalStateEx = (IllegalStateException) exception;
        if (illegalStateEx.getCause() instanceof ProcessingException) {
          final ProcessingException processingException = (ProcessingException) illegalStateEx.getCause();
          log.error(processingException.getMessage(), processingException);
          return Response.status(Status.SERVICE_UNAVAILABLE).entity(
              new JsonObject().put("message", "Um provedor dados se encontra indisponível. Contate o suporte.")).build();
        }
      }
      
      log.error(exception.toString(), exception);

      if (exception instanceof TimeoutException) {
          return Response.status(Status.GATEWAY_TIMEOUT).entity(
                  new JsonObject().put("message", "Endpoint request timed out.")).build();
      }

      return Response.status(Status.INTERNAL_SERVER_ERROR).entity(
          new JsonObject().put("message", "Erro ao processar requisição. Contate o suporte.")).build();
    }
  }

  @Provider
  public static class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {
    @Override
    public Response toResponse(UnauthorizedException exception) {
      log.warn(exception.toString());
      return Response.status(Status.UNAUTHORIZED).entity(new JsonObject().put("message", "Sem autorização.")).build();
    }
  }
  
  @Provider
  public static class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {
    @Override
    public Response toResponse(ForbiddenException exception) {
      log.warn(exception.toString());
      return Response.status(Status.FORBIDDEN).entity(new JsonObject().put("message", "Sem permissão.")).build();
    }
  }
  
}
