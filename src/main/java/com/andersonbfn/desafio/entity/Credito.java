package com.andersonbfn.desafio.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * Entidade representante do contexto de "Credito". 
 * Padrão "Active Record Pattern" (fields públicos)
 * 
 * @author anderson.bernardo
 */
@Entity
@Table(name = "credito")
public class Credito extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;
    
    @Column(name = "limite_credito")
    public BigDecimal limiteCredito;
    
    @ManyToOne
    @JoinColumn(name = "id_cadastrante", referencedColumnName = "id")
    public Usuario cadastrante;

    @Column(name = "aprovada")
	public Boolean isAprovada = Boolean.FALSE; // Por padrão é inicialmente proposta.
    
    @ManyToOne
    @JoinColumn(name = "id_aprovador", referencedColumnName = "id")
    public Usuario aprovador;
    
}
