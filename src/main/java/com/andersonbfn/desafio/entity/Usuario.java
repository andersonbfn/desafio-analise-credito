/**
 * 
 */
package com.andersonbfn.desafio.entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * Entidade representante de "Usuario's" do sistema. 
 * Padrão "Active Record Pattern" (fields públicos)
 * 
 * @author anderson.bernardo
 */
public class Usuario extends PanacheEntityBase {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;
	
	@Column(name = "nome")
	public String nome;
	
	@Column(name = "cpf")
	public String cpf;

	@Enumerated(EnumType.STRING)
	@Column(name = "permissao")
	public Permissao permissao; // Considerando que cada usuario só tenha no máx. 1 permissão.
	
}
