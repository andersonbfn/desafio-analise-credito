package com.andersonbfn.desafio;

import java.io.IOException;
import java.security.Permission;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.Logger;

import com.andersonbfn.desafio.entity.Permissao;
import com.andersonbfn.desafio.entity.Usuario;

import io.quarkus.security.credential.Credential;
import io.quarkus.security.credential.TokenCredential;
import io.quarkus.security.identity.CurrentIdentityAssociation;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.runtime.QuarkusPrincipal;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;

@PreMatching
@Priority(Priorities.USER + 1)
@Provider
public class AuthContainerRequestFilter implements ContainerRequestFilter {
	private static final Logger log = Logger.getLogger(AuthContainerRequestFilter.class);

	@Inject
	SecurityIdentity old;

	@Inject
	CurrentIdentityAssociation currentIdentityAssociation;

	@Context
	UriInfo info;

	@Context
	HttpServerRequest request;

	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {
		log.debug("AuthContainerRequestFilter.filter starting.");

		final String headerAuthorization = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		
		log.debugf("AuthContainerRequestFilter.filter header Authorization: %s", headerAuthorization);
		
		// Pra fins de praticidade não será tratadas segurança a nivel de OAuth2/OpenId ou JWT, mas "token opaco"
		// e coincidentemente cujo valor seja uma permissão válida.
		if (!StringUtils.isEmpty(headerAuthorization) && headerAuthorization.startsWith("Bearer ")) {
			
			final String permissionToken = StringUtils.substringAfter(headerAuthorization, "Bearer ");
			
			final Permissao permissao;
			try {
				permissao = Permissao.valueOf(permissionToken);
			} catch (Exception e) {
				throw new ForbiddenException("Permissão inválida: " + permissionToken, e);
			}
			
			// Melhoria: o usuario poderia ser consultado diretamente de um banco de dados...
			final Usuario usuarioLogado = new Usuario();
			usuarioLogado.nome = "Usuario " + permissao.toString();
			usuarioLogado.permissao = permissao;
			
			final Set<Credential> oldCredentials = old.getCredentials();
			final Map<String, Object> oldAttributes = old.getAttributes();
			final SecurityContext securityContext = requestContext.getSecurityContext();

			final SecurityIdentity newIdentity = new SecurityIdentity() {

				@Override
				public Principal getPrincipal() {
					return new QuarkusPrincipal(usuarioLogado.nome);
				}

				@Override
				public boolean isAnonymous() {
					return permissao == null;
				}

				@Override
				public Set<String> getRoles() {
					if (permissao != null) {
						return new HashSet<String>(Arrays.asList(permissao.toString()));
					}
					return Collections.emptySet();
				}

				@Override
				public boolean hasRole(String role) {
					return permissao.toString().equals(role);
				}

				@Override
				public <T extends Credential> T getCredential(Class<T> credentialType) {
					for (Credential cred : getCredentials()) {
						if (credentialType.isAssignableFrom(cred.getClass())) {
							return (T) cred;
						}
					}
					return null;
				}

				@Override
				public Set<Credential> getCredentials() {
					return Set.of(new TokenCredential(permissao.toString(), "Bearer"));
				}

				@Override
				public <T> T getAttribute(String name) {
					switch (name) {
						case "usuarioLogado":
							return (T) usuarioLogado;
						default:
							break;
					}
					return (T) oldAttributes.get(name);
				}

				@Override
				public Map<String, Object> getAttributes() {
					return oldAttributes;
				}

				@Override
				public Uni<Boolean> checkPermission(Permission permission) {
					return Uni.createFrom().item(permissao.equals(permission));
				}
			};

			currentIdentityAssociation.setIdentity(newIdentity);

		}
			
		log.debug("AuthContainerRequestFilter.filter finished.");
	}
	
}
