package com.andersonbfn.desafio.resource;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.andersonbfn.desafio.entity.Credito;
import com.andersonbfn.desafio.services.CreditoService;

import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;

@Path("/credito")
@Authenticated
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class CreditoResource {

	@Inject
	SecurityIdentity identity;

	@Inject
	CreditoService creditoService;

	@POST
	@RolesAllowed("ATENDIMENTO")
	public Response cadastrar(final Credito body) {
		creditoService.cadastrar(identity, body);
		return Response.status(Status.CREATED).build();
	}

	@PUT
	@Path("/{id}")
	@RolesAllowed("ANALISTA")
	public Response aprovar(@PathParam("id") final Long idProposta) {
		creditoService.aprovar(identity, idProposta);
		return Response.ok().build();
	}

}