package com.andersonbfn.desafio.services;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import org.jboss.logging.Logger;

import com.andersonbfn.desafio.entity.Credito;
import com.andersonbfn.desafio.entity.Usuario;

import io.quarkus.security.identity.SecurityIdentity;

/**
 * Serviço de manutenção de credito.
 * @author anderson.bernardo
 */
@ApplicationScoped
public class CreditoService {
	private static final Logger log = Logger.getLogger(CreditoService.class);

	@Transactional
	public void cadastrar(final SecurityIdentity identity, final Credito proposta) {
		final Usuario usuarioLogado = identity.getAttribute("usuarioLogado");
		log.debugf("CreditoService.cadastrar usuario: %s", usuarioLogado.nome);
		
		// Poderia fazer mappers com map struct aqui para não trabalhar diretamente com entity nos endpoints...
		proposta.cadastrante = usuarioLogado;
		proposta.persist();
		
	}
	
	@Transactional
	public void aprovar(final SecurityIdentity identity, final Long idProposta) {
		final Usuario usuarioLogado = identity.getAttribute("usuarioLogado");
		log.debugf("CreditoService.aprovar usuario: %s", usuarioLogado.nome);
		
		final Optional<Credito> optProposta = Credito.findByIdOptional(idProposta);
		
		if (optProposta.isEmpty()) {
			throw new NotFoundException("Proposta não encontrada. Id: "+ idProposta);
		}
		
		optProposta.ifPresent(proposta -> {
			if (proposta.isAprovada) {
				throw new BadRequestException("A proposta já se encontra aprovada.");
			}
			
			proposta.aprovador = usuarioLogado;
			proposta.isAprovada = Boolean.TRUE;
		});
		
	}
	
}
